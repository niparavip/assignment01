# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Student Details ##
	Name = Nipa Patel
	Student ID = 8778797
	Email = npatel8797@conestogac.on.ca

### What is this repository for? ###

This access control system software codes is a keyless entry system that gives you total control over your commercial or residential premises by granting physical access to 
authorized users only. It is an essential part of any security solution. You can customize this system to fit your specific requirements and easily integrate it with other systems such 
as visitors’ management systems or alarm systems.Quick summary


(https://bitbucket.org/niparavip/assignment01)

### How do I get set up? ###

Steps for project build and installtion

 1.Download the project from bitbucket

 2.Open it in VSCode

 3.Open the terminal

 4.Install npm node

 5.Run the main js using node command




#### License This project is licensed under the MIT License - see the below for details ####

Copyright (c) <2021> <copyright Nipaben Patel>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.